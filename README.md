# tp-init

## 介绍

- 生成基类控制器以及公共函数

## 安装
    composer require shisou/tpinit
## TODO

TP6 的基础控制器

#### 属性

> WHAT? HOW? WHY?  
> _WebController (父类控制器)  
> _Controller (子类控制器)

##### $user

      WHAT: public $user = null  
      HOW： 父控制器  __construct里定义 $user 来获取session('user')   
      WHY： 定义公共 $user，控制器可直接使用方法获取当前session中的用户
      使用示例：$this->user

##### $layout

      WHAT:  protected $layout = '_web'      
      HOW：  父控制器定义 $layout,  新建view() 方法加入  $this->app->view->layout($this->layout)  
      WHY：  其他控制器使用 return $this->view([])时, 此定义的公共模板会被使用。
      使用示例： return $this->view(['data' => $data ])
      
      如果不想使用父控制器定义的 $layout，可在控制器中单独定义另外的公共模板
      $this->layout="_web2"
> 也可使用官方 layout（具体参考手册）

##### $reqId

      WHAT:  protected $reqId = 0  
      HOW： 子类控制器定义 $reqId，initialize() 中加入下方代码
      WHY:  定义此方法可以通过更简洁的代码来直接获取 get 请求的 id。使代码冗余度降低
      使用示例： $this->reqId
>

        if (!($this->reqId = $this->request->post('id'))) {
            $this->reqId = $this->request->get('id');
        }

##### $globals

      WHAT: protected $globals = [];  
      HOW： 子类控制器定义 $globals, 自定控制器可直接通过 $this->global['row'] = ...
      WHY:  使用此格式在返回视图时不需要在单独传入 row，可以理解为返回视图时已将 ['row']带入视图.
      视图直接使用 $row[''] 
      
      使用示例：$this->globals['row'] = User::find(1);
                return $this->view([]);

#### 方法

##### view(){} (父控制器)

      WHAT：
      public function view($view = null, $data = null)
      {
          if (is_array($view) || !$view) {
              $data = $view;
              $view = $this->request->action();
          }
          $data = array_merge($data, $this->globals);

          //        if (!isset($param['layout'])) {
          //            $this->app->view->layout('_web');
          //        }

          return view($view, $data);
      }
      WHY：传入 view data后，取得当前action方法名为视图名字。之后合并返回 view()
##### __construct(){} (子控制器)
      WHAT: define('DATETIME', date('Y-m-d H:i:s')) 
      HOW:  initialize() 定义一个当前时间的常量
      WHY:  自定写法比官方写法相对来说更精短

>

      WHAT: error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
      HOW:  __construct(){} 添加 error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
      WHY: 该函数可设置脚本运行时的级别，如果没有设置就只返回当前错误的级别; 
           当前定义的结果是除了 E_NOTICE和E_WARNING,报告其他所有错误

>  
 
      WHAT：
      define('__CONTROLLER__', str_replace('.', '/', Str::lower($this->request->controller())));
      define('__ACTION__', Str::lower($this->request->action()));
      define('__URL__', __CONTROLLER__ . '/' . __ACTION__);
      HOW： 可以高效快速获取当前请求的控制器，方法以及路径
      WHY： 当需要判断路径，输出路径等可以快速使用查出值

##### response()

      WHAT：
      protected function response($code, $msg, $data, $url)
      {
          $json = [
              'code' => $code,
              'msg'  => $msg,
              'data' => $data,
              'url'  => $url,
            ];

          return json($json);
      }
      WHY：此方法将结果转为json，需与 success fail 共同使用

##### success()

      WHAT：
      protected function success($msg = '', $data = null, $url = '')
      {
          if (is_array($msg)) {
              $data = $msg;
              $msg  = '';
          }

        //        $msg = $msg ?: '成功';

          return $this->response(1, $msg, $data, $url);
      }
      WHY：成功输出; 将传入的值输出json格式并格式化输出结果
      
      // 直接返回
      $this->success()
      // 只返回信息
      $this->success('$msg')
      // 只返回数据
      $this->success('$array')

##### fail()

      WHAT：
      protected function fail($msg, $data = null, $url = '')
      {
          return $this->response(0, $msg, $data, $url);
      }
      WHY：失败输出，格式化输出失败数据

## common
常用的公共函数

##### image()
- 处理七牛云返回的图片
- $imgCover 可以换成自己所需要的路径
- 使用说明：<?=image('图片名称' ,'宽高')?>

      function image($imgCover = '', $size = '')
      {
          if (!$imgCover) {
            $imgCover = 'http://qn1.10soo.net/art/202209211128389427';
          }

          $wh = $size ? explode('x', $size) : '';
    
          $cutting = $wh ? '?imageView2/1/w/' . $wh[0] . '/h/' . $wh[1] . '/q/75' : '';
    
          return $imgCover . $cutting;
      }


##### pagelist()
- 分页查询
- 使用说明：<?php pageList('model', '此处为条件') ?>

       function pageList($model, $where = [], $order = null, $limit = 6)
       {
          $where[] = [
             'status',
             '=',
              1,
          ];

          $order = is_null($order) ? 'id desc' : $order;

          $model = "\\app\\models\\" . $model;

          $list = (new $model)->filterWhere($where)
          ->order($order)
          ->paginate([
              'list_rows' => $limit,
              'var_page'  => 'p',
          ]);

          return $list;
        }

##### show()

- 详情页url
- $param ['id'=>1,'title'=' ']
- view页面 <?=show('')?>

        function show($param = [])
        {
            retrun url('public/details', $param);
        }

##### cateUrl()

- 栏目url
- 使用说明：<?=cateUrl(1);?>

        function cateUrl($cateId = 0)
        {
            $cate = \app\models\Cate::findOrNew($cateId);
            
            return $cate->recUrl();
        }

#####  isWx()

- 判断微信客户端

        function isWx()
        {
            return strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false;
        }


## libs
框架扩展库类
#### Export  

- 生成 exel 文件

#### Page

- 分页功能扩展库

#### Qiniu

- 上传图片 
- 七牛云扩展库

#### Sha1

- 生成加密字符串


