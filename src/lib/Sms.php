<?php

namespace shisou\tpinit\lib;

use Overtrue\EasySms\EasySms;

class Sms
{
    public static $err = '';

    public static function sendCode($mobile, $code)
    {
        $config = config('config');

        try {
            $easySms = new EasySms($config['easysms']);
            $easySms->send($mobile, [
                'data'     => [
                    'code' => $code
                ],
            ]);
        } catch (\Throwable $th) {
            static::$err = $th->getMessage();
            return false;
        }

        return true;
    }
}
